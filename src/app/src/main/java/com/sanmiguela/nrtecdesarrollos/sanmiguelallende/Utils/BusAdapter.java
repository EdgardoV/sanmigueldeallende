package com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Utils;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Objects.Bus;
import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.R;

import java.util.ArrayList;

public class BusAdapter extends RecyclerView.Adapter<BusAdapter.ViewHolder> {

    private static final String TAG = "BUSADAPTER";
    public final Context mContext;
    public final ArrayList<Bus> mEconomicos;

    public BusAdapter(Context mContext, ArrayList<Bus> mEconomicos) {
        this.mContext = mContext;
        this.mEconomicos = mEconomicos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d(TAG,"onCreateViewHolder: called.");

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bus,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Bus eco = mEconomicos.get(i);

        viewHolder.txtArrived.setText("00:00 AM");
        viewHolder.txtBusNumber.setText("N° "+eco.numeroEco);
        viewHolder.txtCapacity.setText(eco.ocupacionEco+"%");

        /*viewHolder.btnBus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Seleccionaste el economico "+eco.numeroEco, Toast.LENGTH_LONG).show();
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return this.mEconomicos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgBus;
        TextView txtBusNumber, txtCapacity, txtArrived;
        ConstraintLayout backgroundBusCapacity;
        Button btnBus;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgBus = (ImageView) itemView.findViewById(R.id.imgBusItem);
            txtBusNumber = (TextView) itemView.findViewById(R.id.txtBusNumberItem);
            txtCapacity = (TextView) itemView.findViewById(R.id.txtCapacityItem);
            txtArrived = (TextView) itemView.findViewById(R.id.txtArrivedItem);
            backgroundBusCapacity = (ConstraintLayout) itemView.findViewById(R.id.backgroundBusItem);
            btnBus = (Button) itemView.findViewById(R.id.btnBusItem);
        }
    }

}
