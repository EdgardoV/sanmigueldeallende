package com.sanmiguela.nrtecdesarrollos.sanmiguelallende;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import static java.lang.Thread.sleep;

public class LoginActivity extends AppCompatActivity {

    Button siguiente,ingresa;
    ImageButton back;
    EditText user, pass;
    TextView txt_bienvenido;
    View logo_main,img_sanmiguel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        siguiente = (Button) findViewById(R.id.siguiente);
        ingresa = (Button) findViewById(R.id.ingresa);

        back = (ImageButton) findViewById(R.id.back);

        user = (EditText) findViewById(R.id.user);
        pass = (EditText) findViewById(R.id.pass);

        txt_bienvenido = (TextView) findViewById(R.id.bienvenido);

        img_sanmiguel = (View) findViewById(R.id.imageView2);

        logo_main = (View) findViewById(R.id.logo_main);

        if(ingresa != null) {
            ingresa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendToMain();
                }
            });
        }
        setElementAlpha();

    }

    @Override
    protected void onStart() {
        super.onStart();

        startAnimations();
    }

    private void sendToMain() {
        Intent intent = new Intent(LoginActivity.this, ScheduleActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }


    public void back_pressed(View view){
        user.setVisibility(View.VISIBLE);
        siguiente.setVisibility(View.VISIBLE);

        back.animate().alpha(0).setDuration(100).withEndAction(new Runnable() {
            @Override
            public void run() {
                pass.animate().alpha(0).setDuration(100);
                ingresa.animate().alpha(0).setDuration(100).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        back.setVisibility(View.INVISIBLE);
                        ingresa.setVisibility(View.INVISIBLE);
                        user.animate().alpha(1).setDuration(100);
                        siguiente.animate().alpha(1).setDuration(100);
                        pass.setVisibility(View.INVISIBLE);
                    }
                }).start();
            }
        }).start();
    }


    public void siguiente_pressed(View view){
        pass.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);
        ingresa.setVisibility(View.VISIBLE);
        user.animate()
                .alpha(0)
                .setDuration(100)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        siguiente.animate()
                                .alpha(0)
                                .setDuration(100)
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        user.setVisibility(View.INVISIBLE);
                                        pass.setVisibility(View.VISIBLE);
                                        pass.animate().alpha(1).setDuration(100);
                                        siguiente.setVisibility(View.INVISIBLE);
                                        ingresa.setVisibility(View.VISIBLE);
                                        ingresa.animate().alpha(1).setDuration(100);
                                        back.setVisibility(View.VISIBLE);
                                        back.animate().alpha(1).setDuration(100);
                                    }
                                }).start();
                    }
                })
                .start();


    }

    private void setElementAlpha() {
        logo_main.setAlpha(0);
        txt_bienvenido.setAlpha(0);
        user.setAlpha(0);
        pass.setAlpha(0);
        //ingresa.setAlpha(0);
        img_sanmiguel.setAlpha(0);
        //back.setImageAlpha(0);
        siguiente.setAlpha(0);
    }

    private void startAnimations() {
        logo_main.animate().alpha(1).setDuration(100).withEndAction(new Runnable() {
            @Override
            public void run() {
                txt_bienvenido.animate().alpha(1).setDuration(100).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        user.animate().alpha(1).setDuration(100).start();
                        pass.animate().alpha(1).setDuration(100).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                siguiente.animate().alpha(1).setDuration(100).start();
                                img_sanmiguel.animate().alpha(1).setDuration(100).start();
                            }
                        }).start();
                    }
                }).start();
            }
        }).start();
    }

}
