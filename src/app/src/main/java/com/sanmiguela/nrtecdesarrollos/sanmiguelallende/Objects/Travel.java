package com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Objects;

public class Travel {

    public int travelId;
    public String travelName;
    public String travelStart;
    public String travelEnd;

    public Travel(int travelId, String travelName, String travelStart, String travelEnd) {
        this.travelId = travelId;
        this.travelName = travelName;
        this.travelStart = travelStart;
        this.travelEnd = travelEnd;
    }
}
