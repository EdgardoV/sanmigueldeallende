package com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Objects;

import java.util.ArrayList;

public class Schedule {

    public long idHorario;
    public String horario;
    public long capacidadHorario;
    public long pasajeHorario;
    public long ocupacionHorario;
    public ArrayList<Bus> economicosHorario;

    public Schedule(long idHorario, String horario, long capacidadHorario, long pasajeHorario, ArrayList<Bus> economicosHorario) {
        this.idHorario = idHorario;
        this.horario = horario;
        this.capacidadHorario = capacidadHorario;
        this.pasajeHorario = pasajeHorario;
        this.economicosHorario = economicosHorario;
        this.ocupacionHorario = 17;
    }
}
