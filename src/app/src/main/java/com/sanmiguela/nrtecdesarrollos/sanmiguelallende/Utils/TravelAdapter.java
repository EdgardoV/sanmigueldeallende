package com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Utils;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Objects.Travel;
import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.R;

import java.util.ArrayList;

public class TravelAdapter extends RecyclerView.Adapter<TravelAdapter.ViewHolder> {

    private static final String TAG = "TRAVELADAPTER";
    public Context mContext;
    public ArrayList<Travel> mTravels;

    public TravelAdapter(Context mContext, ArrayList<Travel> mTravels) {
        this.mContext = mContext;
        this.mTravels = mTravels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG,"onCreateViewHolder: called.");

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_travel,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Travel travel = mTravels.get(position);

        holder.txtTravelName.setText(travel.travelName);
        holder.txtTravelStart.setText(travel.travelStart);
        holder.txtTravelEnd.setText(travel.travelEnd);

    }

    @Override
    public int getItemCount() {
        return mTravels.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtTravelName, txtTravelStart, txtTravelEnd;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTravelName = (TextView) itemView.findViewById(R.id.txtTravelName);
            txtTravelStart = (TextView) itemView.findViewById(R.id.txtTravelTimeDeparture);
            txtTravelEnd = (TextView) itemView.findViewById(R.id.txtTravelTimeArrived);


        }
    }

}
