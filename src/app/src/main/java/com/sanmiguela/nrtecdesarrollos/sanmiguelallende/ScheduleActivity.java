package com.sanmiguela.nrtecdesarrollos.sanmiguelallende;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.maps.model.LatLng;
import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Extras.RecyclerItemClickListener;
import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Objects.Bus;
import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Utils.BusAdapter;

import java.util.ArrayList;

public class ScheduleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_schedule);

        initScheduleView();
    }

    private void initScheduleView(){

        ArrayList<Bus> economicos2 = new ArrayList<>();

        economicos2.add(new Bus(
                00003,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000031,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000032,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                00003,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000031,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000032,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                00003,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000031,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000032,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                00003,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000031,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000032,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000032,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                00003,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000031,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        economicos2.add(new Bus(
                000032,
                50.0,
                49,
                new LatLng(20.000,-100.00),
                45.0

        ));

        LinearLayoutManager layoutManager_bus = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        final RecyclerView BusViewer = (RecyclerView) findViewById(R.id.economicos);
        BusViewer.setLayoutManager(layoutManager_bus);
        BusAdapter adapter_bus = new BusAdapter(ScheduleActivity.this, economicos2);
        BusViewer.setAdapter(adapter_bus);

        BusViewer.addOnItemTouchListener(new RecyclerItemClickListener(this, BusViewer, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(ScheduleActivity.this, TicketActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

    }
}
