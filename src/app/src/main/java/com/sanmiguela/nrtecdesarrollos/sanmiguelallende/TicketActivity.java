package com.sanmiguela.nrtecdesarrollos.sanmiguelallende;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class TicketActivity extends AppCompatActivity implements View.OnClickListener {

    private int[] nTickets;
    private Double[] ticketsPrices;
    private Double totalPrice;

    Button btnConfirmTicket;
    Button btnAddINSEN, btnAddAdult, btnAddChild, btnAddDisc;
    Button btnRemoveINSEN, btnRemoveAdult, btnRemoveChild, btnRemoveDisc;
    TextView actualDate, actualHour;
    TextView txtDeparture, txtBusNumber, txtArrived;
    TextView ticketsINSEN, ticketsAdult, ticketsChild, ticketsDisc;
    TextView totalTicketsINSEN, totalTicketsAdult, totalTicketsChild, totalTicketsDisc;
    TextView txtTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ticket);

        nTickets = new int[]{0, 0, 0, 0};
        ticketsPrices = new Double[]{1.0,1.0,1.0,1.0};
        totalPrice = 0.0;

        initViewElements();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btnAddINSEN:
                nTickets[0]++;
                ticketsINSEN.setText(String.format("%02d", nTickets[0]));
                setTotalPrice(ticketsPrices[0]);
                break;
            case R.id.btnRemoveINSEN:
                if(nTickets[0] > 0){
                    nTickets[0]--;
                    ticketsINSEN.setText(String.format("%02d", nTickets[0]));
                    setTotalPrice((-1)*(ticketsPrices[0]));
                }
                break;
            case R.id.btnAddAdult:
                nTickets[1]++;
                ticketsAdult.setText(String.format("%02d", nTickets[1]));
                setTotalPrice(ticketsPrices[1]);
                break;
            case R.id.btnRemoveAdult:
                if(nTickets[1] > 0){
                    nTickets[1]--;
                    ticketsAdult.setText(String.format("%02d", nTickets[1]));
                    setTotalPrice((-1)*(ticketsPrices[1]));
                }
                break;
            case R.id.btnAddChild:
                nTickets[2]++;
                ticketsChild.setText(String.format("%02d", nTickets[2]));
                setTotalPrice(ticketsPrices[2]);
                break;
            case R.id.btnRemoveChild:
                if(nTickets[2] > 0){
                    nTickets[2]--;
                    ticketsChild.setText(String.format("%02d", nTickets[2]));
                    setTotalPrice((-1)*(ticketsPrices[2]));
                }
                break;
            case R.id.btnAddDisc:
                nTickets[3]++;
                ticketsDisc.setText(String.format("%02d", nTickets[3]));
                setTotalPrice(ticketsPrices[3]);
                break;
            case R.id.btnRemoveDisc:
                if(nTickets[3] > 0){
                    nTickets[3]--;
                    ticketsDisc.setText(String.format("%02d", nTickets[3]));
                    setTotalPrice((-1)*(ticketsPrices[3]));
                }
                break;
            case R.id.btnConfirmTicket:

                Intent intent = new Intent(TicketActivity.this, ConfirmationActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

                break;
        }

    }

    public void setTotalPrice(Double amount){
        totalPrice = totalPrice + amount;
        txtTotal.setText("$"+String.format("%.2f", totalPrice));
    }

    public void initViewElements(){

        actualDate = (TextView) findViewById(R.id.fecha_seleccionada);
        actualHour = (TextView) findViewById(R.id.hora_actual);

        txtDeparture = (TextView) findViewById(R.id.txtDepartureSelected);
        txtBusNumber = (TextView) findViewById(R.id.txtBusNumberSelected);
        txtArrived = (TextView) findViewById(R.id.txtArrivedSelected);

        btnAddINSEN = (Button) findViewById(R.id.btnAddINSEN);
        btnAddAdult = (Button) findViewById(R.id.btnAddAdult);
        btnAddChild = (Button) findViewById(R.id.btnAddChild);
        btnAddDisc = (Button) findViewById(R.id.btnAddDisc);
        btnRemoveINSEN = (Button) findViewById(R.id.btnRemoveINSEN);
        btnRemoveAdult = (Button) findViewById(R.id.btnRemoveAdult);
        btnRemoveChild = (Button) findViewById(R.id.btnRemoveChild);
        btnRemoveDisc = (Button) findViewById(R.id.btnRemoveDisc);

        ticketsINSEN = (TextView) findViewById(R.id.boletos_insen);
        ticketsAdult = (TextView) findViewById(R.id.boletos_adulto);
        ticketsChild = (TextView) findViewById(R.id.boletos_menor);
        ticketsDisc = (TextView) findViewById(R.id.boletos_disc);

        totalTicketsINSEN = (TextView) findViewById(R.id.txtTotalINSEN);
        totalTicketsAdult = (TextView) findViewById(R.id.txtTotalAdulto);
        totalTicketsChild = (TextView) findViewById(R.id.txtTotalMenor);
        totalTicketsDisc = (TextView) findViewById(R.id.txtTotalDisc);

        txtTotal = (TextView) findViewById(R.id.txtTotalPrice);
        btnConfirmTicket = (Button) findViewById(R.id.btnConfirmTicket);

        btnAddINSEN.setOnClickListener(this);
        btnAddAdult.setOnClickListener(this);
        btnAddChild.setOnClickListener(this);
        btnAddDisc.setOnClickListener(this);
        btnRemoveINSEN.setOnClickListener(this);
        btnRemoveAdult.setOnClickListener(this);
        btnRemoveChild.setOnClickListener(this);
        btnRemoveDisc.setOnClickListener(this);
        btnConfirmTicket.setOnClickListener(this);

    }
}
