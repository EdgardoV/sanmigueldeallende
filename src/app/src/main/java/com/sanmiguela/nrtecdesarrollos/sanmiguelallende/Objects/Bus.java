package com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Objects;

import com.google.android.gms.maps.model.LatLng;

public class Bus {

    /**
     * Clase que contiene la informacion del Economico que realizara el viaje
     */

    public long numeroEco;
    public Double capacidadEco;
    public Double pasajeEco;
    public long tiempoEconomico;
    public LatLng ubicacionEco;
    public Double ocupacionEco;

    public Bus(long numeroEco, Double capacidadEco, long tiempoEconomico, LatLng ubicacionEco, Double pasajeEco) {
        this.numeroEco = numeroEco;
        this.capacidadEco = capacidadEco;
        this.tiempoEconomico = tiempoEconomico;
        this.ubicacionEco = ubicacionEco;
        this.pasajeEco = pasajeEco;
        this.ocupacionEco = (pasajeEco/capacidadEco)*100;
    }
}
