package com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Utils;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.Objects.Schedule;
import com.sanmiguela.nrtecdesarrollos.sanmiguelallende.R;

import java.util.ArrayList;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<Schedule> mHorarios = new ArrayList<>();
    private final Context mContext;

    public ScheduleAdapter(Context mContext, ArrayList<Schedule> mHorarios) {
        this.mHorarios = mHorarios;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d(TAG,"onCreateViewHolder: called.");

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_schedule,viewGroup,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,final int i) {
        Log.d(TAG, "onBindViewHolder: called.");
        Schedule horario = mHorarios.get(i);


        viewHolder.txtCapacity.setText(horario.horario);

        //if(Integer.parseInt(mCapacidad.get(i)) < 50){
            viewHolder.backgroundCapacity.setBackgroundResource(R.drawable.btn_avaible_hour_green);
        /*}else if (Integer.parseInt(mCapacidad.get(i)) >= 50 && Integer.parseInt(mCapacidad.get(i)) < 70){
            viewHolder.backgroundCapacity.setBackgroundResource(R.drawable.btn_avaible_hour_yellow);
        }else if (Integer.parseInt(mCapacidad.get(i)) >= 70 && Integer.parseInt(mCapacidad.get(i)) < 90){
            viewHolder.backgroundCapacity.setBackgroundResource(R.drawable.btn_avaible_hour_orange);
        }else if (Integer.parseInt(mCapacidad.get(i)) >= 90 && Integer.parseInt(mCapacidad.get(i)) <= 100){
            viewHolder.backgroundCapacity.setBackgroundResource(R.drawable.btn_avaible_hour_red);
        }*/

        viewHolder.capacidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG,"Hour selected: "+ mHorarios.get(i).horario);
                //Toast.makeText(mContext,"Clicked Button"+i,Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mHorarios.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        Button capacidad;
        TextView txtCapacity;
        ConstraintLayout backgroundCapacity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            capacidad = itemView.findViewById(R.id.capacity);
            txtCapacity = itemView.findViewById(R.id.txtCapacity);
            backgroundCapacity = itemView.findViewById(R.id.backgroundCapacity);

        }
    }

}
